# Mirage (Vagrant + Tk) Prototype
This is a prototype utilizing Vagrant (for VM provisioning), Tk (for Ruby GUI stuff), and VirtualBox (for the VM).

##Installation
1. Install Git (c'mon)
2. Install Ruby
3. Install Tk gem `gem install tk`
4. Install VirtualBox
5. Install Vagrant
6. `git clone https://[your username]@bitbucket.org/jcleeterp/mirage-tk-prototype.git`

##Run
- Go into the project folder, run `run.sh`, and be patient.
- On Windows, just go into the folder and double-click `run.sh`
- On Linux, cd into the folder and run `sh run.sh`

##Inside The VM
1. Login as
user: `vagrant`
pass: `vagrant`
2. Run `curl ipinfo.io` to verify you appear at the VPN location.
