require 'tk'

root = TkRoot.new { title "Mirage" }
TkLabel.new(root) do
   text 'From what location would you like to appear through the VPN?'
   pack { padx 15 ; pady 15; side 'left' }
end

combobox = TkCombobox.new(root)
combobox.values = ["Canada", "Germany"]
combobox.current = 0
combobox.place('height' => 25, 
               'width'  => 100, 
               'x'   => 15, 
               'y'   => 30 )

start = TkButton.new(root) do
  text "Start"
  borderwidth 5
  underline 0
  state "normal"
  command (proc {
    if Gem.win_platform? then
      head = "copy /Y "
    else
      head = "cp "
    end
    if combobox.get == "Canada" then
      tail = "canada.ovpn vpn.ovpn"
    elsif combobox.get == "Germany" then
      tail = "germany.ovpn vpn.ovpn"
    end
    system(head + tail)
    root.destroy()
  })
  pack("side" => "right",  "padx"=> "50", "pady"=> "10")
end

Tk.mainloop
